# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'purevpn.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.setWindowModality(QtCore.Qt.NonModal)
        Dialog.resize(1000, 700)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("logo.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.verticalLayout = QtGui.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.pushRefresh = QtGui.QPushButton(Dialog)
        self.pushRefresh.setObjectName(_fromUtf8("pushRefresh"))
        self.verticalLayout.addWidget(self.pushRefresh)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.radioTCP = QtGui.QRadioButton(Dialog)
        self.radioTCP.setToolTip(_fromUtf8(""))
        self.radioTCP.setChecked(True)
        self.radioTCP.setObjectName(_fromUtf8("radioTCP"))
        self.horizontalLayout_3.addWidget(self.radioTCP)
        self.radioUDP = QtGui.QRadioButton(Dialog)
        self.radioUDP.setObjectName(_fromUtf8("radioUDP"))
        self.horizontalLayout_3.addWidget(self.radioUDP)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.tableServers = QtGui.QTableView(Dialog)
        self.tableServers.setEditTriggers(QtGui.QAbstractItemView.CurrentChanged|QtGui.QAbstractItemView.DoubleClicked)
        self.tableServers.setDragDropMode(QtGui.QAbstractItemView.NoDragDrop)
        self.tableServers.setAlternatingRowColors(True)
        self.tableServers.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.tableServers.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.tableServers.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.tableServers.setObjectName(_fromUtf8("tableServers"))
        self.verticalLayout.addWidget(self.tableServers)
        self.pushConnect = QtGui.QPushButton(Dialog)
        self.pushConnect.setEnabled(False)
        self.pushConnect.setCheckable(False)
        self.pushConnect.setChecked(False)
        self.pushConnect.setObjectName(_fromUtf8("pushConnect"))
        self.verticalLayout.addWidget(self.pushConnect)
        self.pushDisconnect = QtGui.QPushButton(Dialog)
        self.pushDisconnect.setEnabled(False)
        self.pushDisconnect.setObjectName(_fromUtf8("pushDisconnect"))
        self.verticalLayout.addWidget(self.pushDisconnect)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.labelStatus = QtGui.QLabel(Dialog)
        self.labelStatus.setText(_fromUtf8(""))
        self.labelStatus.setObjectName(_fromUtf8("labelStatus"))
        self.horizontalLayout.addWidget(self.labelStatus)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.labelUpdate = QtGui.QLabel(Dialog)
        self.labelUpdate.setText(_fromUtf8(""))
        self.labelUpdate.setObjectName(_fromUtf8("labelUpdate"))
        self.horizontalLayout_2.addWidget(self.labelUpdate)
        self.progressUpdate = QtGui.QProgressBar(Dialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.progressUpdate.sizePolicy().hasHeightForWidth())
        self.progressUpdate.setSizePolicy(sizePolicy)
        self.progressUpdate.setProperty("value", 26)
        self.progressUpdate.setObjectName(_fromUtf8("progressUpdate"))
        self.horizontalLayout_2.addWidget(self.progressUpdate)
        self.horizontalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "OpenVPN setup using PureVPN servers", None))
        self.pushRefresh.setText(_translate("Dialog", "Refresh PureVPN Servers", None))
        self.radioTCP.setText(_translate("Dialog", "TCP", None))
        self.radioUDP.setText(_translate("Dialog", "UDP", None))
        self.pushConnect.setText(_translate("Dialog", "Connect", None))
        self.pushDisconnect.setText(_translate("Dialog", "Disconnect", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

