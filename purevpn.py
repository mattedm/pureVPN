#!/usr/bin/python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 21 17:33:29 2017

@author: mremington
"""
import purevpnui as gui
import ping
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QTimer
import os
import subprocess
from os import listdir
from os.path import isfile, join
import re

import fcntl
import table

def get_pid(name):
    try:
        return subprocess.check_output(["pidof",name])
    except:
        return ''

def nonBlockRead(output):
    fd = output.fileno()
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
    try:
        return output.read()
    except:
        return ''

def tableserverselect(item1,item2):
    print ("Tableserverselect:", item1)

class VPN_App(gui.Ui_Dialog):

    def __init__(self,Dialog):
        super(VPN_App,self).__init__()
        self.setupUi(Dialog)
        self.dialog = Dialog
        self.timer = QTimer()
        self.timer.timeout.connect(self.tick)
        self.timer.start(300)
        self.VPNprocess = subprocess.Popen("pwd")
        self.path = './Linux OpenVPN Updated files/TCP/'
        self.readbytes = 0
        self.writebytes = 0
        self.PID = 0
        self.state = 0
        self.selectedfile = None
        self.progressUpdate.setVisible(False)
        self.hostupdate = False
        
        self.pushRefresh.clicked.connect(self.refresh_servers)
        self.radioTCP.clicked.connect(self.radioTcpUdpActivate)      
        self.radioUDP.clicked.connect(self.radioTcpUdpActivate)
        self.pushConnect.clicked.connect(self.connectVPN)
        self.pushDisconnect.clicked.connect(self.disconnectVPN)
        self.tableServers.clicked.connect(self.serverrowselect)
        
        self.pinged = 0
        self.hosts=[]
        self.tablemodel = table.TableModel()
        
        self.tablemodel.update(self.hosts)
        self.tableServers.setModel(self.tablemodel)
        self.tableServers.setColumnWidth(0,150)
        self.tableServers.setColumnWidth(1,300)
        self.tableServers.setColumnWidth(1,250)
        hh = self.tableServers.horizontalHeader()
        hh.setStretchLastSection(True)
        self.tableServers.setSortingEnabled(True)
        self.radioTcpUdpActivate()
        
    def disconnectVPN(self):
        check = subprocess.check_call(["killall", "openvpn"])
        
        #while str(get_pid("openvpn")).split() is not []:
        Pids = str(get_pid("openvpn")).split()
        while Pids <> []:
            Pids = str(get_pid("openvpn")).split()
        
    def serverrowselect(self, index):
        print("current row is {0}".format(index.row()))
        self.selectedfile = self.hosts[index.row()].path
        ui.pushConnect.setEnabled(True)
    
    def radioTcpUdpActivate(self):
        self.pinged = 0
        self.pushConnect.setEnabled(False)
        if self.radioTCP.isChecked():
            self.path = './Linux OpenVPN Updated files/TCP/'
        else:
            self.path = './Linux OpenVPN Updated files/UDP/'
        try:
            onlyfiles = [f for f in listdir(self.path) if isfile(join(self.path, f))]
    
            self.hosts= []
            self.tablemodel.layoutAboutToBeChanged.emit()
            for f in onlyfiles:
                host = ping.Host(str(f).split('-')[0],self.path + f,self.update)
                self.hosts.append(host)
            self.hostupdate = True        
            self.progressUpdate.setValue(0)
            self.progressUpdate.setVisible(True)
            self.tablemodel.update(self.hosts)
            self.tableServers.setModel(self.tablemodel)
            self.tablemodel.layoutChanged.emit()
            ping.concurrentPing(self.hosts,self.finished)
        except:
            print 'No files downloaded'
    
    def refresh_servers(self):
        try:
            print ("Refresh button pressed")
            check = subprocess.check_call(["wget", "-N", "https://s3-us-west-1.amazonaws.com/heartbleed/linux/linux-files.zip"])
            check = subprocess.check_call(["unzip", "-o", "linux-files.zip"])
            self.radioTcpUdpActivate()
        except ValueError:
            print ("Refresh Failed!")
        except subprocess.CalledProcessError:
            print ("Process error")
            
    def connectVPN(self):
        PIDs = str(get_pid("openvpn")).split()
        if len(PIDs) > 0:
            print ("Already Running!")
    
            # Show a message box
            result = QtGui.QMessageBox.question(self.dialog, 'Message', "An instance of OpenVPN is already running, would you like to kill it?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
    
            if result == QtGui.QMessageBox.Yes:
                self.disconnectVPN()
            else:
                pass


        server =  self.hosts[self.tableServers.selectedIndexes()[0].row()].path
        print server
        
        process = ['openvpn --config "' +server+  '" --float --ca "./Linux OpenVPN Updated files/ca.crt" --auth-user-pass userpass --tls-auth "./Linux OpenVPN Updated files/Wdc.key"']
        self.VPNprocess = subprocess.Popen(process,bufsize=1,shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        print ("PID: {0}".format(self.VPNprocess.pid))            


    def tick(self):
        
        if self.hostupdate is True:
            progress = int((float(self.pinged)/len(self.hosts))*100)
            self.progressUpdate.setValue(progress)        
               
        PIDs = str(get_pid("openvpn")).split()
        if PIDs == []:
            self.labelStatus.setText("")
            self.pushDisconnect.setEnabled (False)
            return
    
        self.pushDisconnect.setEnabled (True)
        if self.VPNprocess.stdout is None:
            return
        if self.PID != PIDs[0]:
            self.PID = PIDs[0]
            self.state = 0
    
        for processID in PIDs :
            print ("PIDOUT {0}".format(processID))
            subprocess.call(['kill','-s', 'SIGUSR2',processID])
        while True:
            line = nonBlockRead(self.VPNprocess.stdout)
            if line != '':
    
                tokens = str(line.rstrip())
                print (tokens)
                match = re.search('Initialization Sequence Completed', tokens)
                if match:
                    self.state = 1
                match = re.search('TUN/TAP read bytes,(\d+)', tokens)
                if match:
                    self.readbytes = int(match.group(1))
                match = re.search('TUN/TAP write bytes,(\d+)', tokens)
                if match:
                    self.writebytes = int(match.group(1))
                    if self.state == 1:
                        self.labelStatus.setText("Connected: {:,} bytes read/{:,} bytes write".format(self.readbytes,self.writebytes))
                    else:
                        self.labelStatus.setText("Initializing")
            else:
                break

    def update(self):
        self.tablemodel.layoutAboutToBeChanged.emit()
        self.tablemodel.layoutChanged.emit()
        self.pinged+=1

    def finished(self,var1):
        print ("Update Finished!")
        self.hostupdate = False
        self.progressUpdate.setVisible(False)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = VPN_App(Dialog)
    
    #radioTcpUdpActivate()

    
    #print "Selection Model:",ui.tableServers.selectionModel()    

    
    Dialog.show()
    
    sys.exit(app.exec_())
 