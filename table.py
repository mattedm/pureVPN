# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 21:35:52 2017

@author: mremington
"""
import operator
from PyQt4 import QtCore, QtGui

class TableModel(QtCore.QAbstractTableModel): 
    def __init__(self, parent=None, *args): 
        super(TableModel, self).__init__()
        self.datatable = None

    def update(self, hosts):
        print ('Updating Model')
        self.datatable = hosts
        #print 'Datatable : {0}'.format(self.datatable)

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.datatable)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return 4
        
        
    def sort(self, col, order):
        """sort table by given column number col"""
        # print(">>> sort() col = ", col)
        
        self.layoutAboutToBeChanged.emit()
        if col == 0:
            self.datatable = sorted(self.datatable, key=lambda x: x.name)
        elif col == 1:
            self.datatable = sorted(self.datatable, key=lambda x: x.domain)
        elif col == 2:
            self.datatable = sorted(self.datatable, key=lambda x: x.ping_result if x.ping_result is not None else float("inf") )
        elif col == 3:
            self.datatable = sorted(self.datatable, key=lambda x: x.path)
        if order == QtCore.Qt.DescendingOrder:
            self.datatable.reverse()
        self.layoutChanged.emit()        

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            if index.column() == 0:
                return '{0}'.format(self.datatable[index.row()].name)
            elif index.column() == 1:
                return '{0}'.format(self.datatable[index.row()].domain)
            elif index.column() == 2:
                return '{0}'.format(self.datatable[index.row()].ping_result)
            elif index.column() == 3:
                return '{0}'.format(self.datatable[index.row()].path)
        else:
            return None

    def headerData(self, col, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            if col == 0:
                return "Name"
            if col == 1:
                return "Host"
                #return QtCore.QVariant(str("Hostname"))
            if col == 2:
                return "Ping Average"                
                #return QtCore.QVariant(str("Ping Average"))        
            if col == 3:
                return "Path"
                #return QtCore.QVariant(str("Path"))
        return None
 
    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        