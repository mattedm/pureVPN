# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 18:32:02 2017

@author: mremington
"""
import subprocess
import os
from multiprocessing.pool import ThreadPool
from multiprocessing import Pool
pool = None
class Host(object):
    def __init__(self, name,path,callbackdone):
        self.name = name
        self.domain = None
        self.ip = '0.0.0.0'
        self.ping_result = None
        self.path = path
        self.returnfunc = callbackdone

def hostlook(host):
    f = open(host.path,'r')
    x=0
    while x<8:
        text = f.readline()
        if 'remote' in text:
            host.domain = (text.split())[1]
            pingavg(host)
        x+=1    

def pingavg(host):
    try:
        numPings = 4
        pingTimeout = 10
        maxWaitTime = 30
        
        output = subprocess.check_output('ping -c %s -w %s %s' % (numPings, (pingTimeout), host.domain), shell=True)
        output = output.split('\n')[-3:]
        # -1 is a blank line, -3 & -2 contain the actual results
        
        xmit_stats = output[0].split(",")
        timing_stats = output[1].split("=")[1].split("/")
        
        packet_loss = float(xmit_stats[2].split("%")[0])
        
        ping_min = float(timing_stats[0])
        ping_avg = float(timing_stats[1])
        ping_max = float(timing_stats[2])
        #print "Ping found!",host.name, ping_avg
        host.ping_result = ping_avg
        host.returnfunc()
    except:
        host.ping_result = None
        host.returnfunc()
        #print host.name
 
def concurrentPing(hosts,callbackfunc):
    pool = ThreadPool(processes=200)
#    pool.map
    pool.map_async(hostlook, hosts, callback = callbackfunc)
    return
    
